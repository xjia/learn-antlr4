import java.util.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class FreeVariableChecker extends MiniHaskellBaseVisitor<Boolean> {

    static class Scope {
        String name;
        Scope parent;
        Set<String> vars;
        Set<String> freeVars;
        Scope(String name, Scope parent) {
            this.name = name;
            this.parent = parent;
            this.vars = new HashSet<String>();
            this.freeVars = new HashSet<String>();
        }
        String getName() { return name; }
        Scope getParent() { return parent; }
        boolean isDefined(String var) {
            if (contains(var)) return true;
            if (parent == null) return false;
            return parent.isDefined(var);
        }
        boolean contains(String var) { return vars.contains(var); }
        void put(String var) { vars.add(var); }
        void addFreeVar(String var) { freeVars.add(var); }
        Set<String> getFreeVars() { return freeVars; }
        @Override
        public String toString() {
            Map map = new HashMap();
            map.put("name", name);
            map.put("vars", vars);
            map.put("free", freeVars);
            return map.toString();
        }
    }

    private Scope currentScope = null;

    private String getScopeName() {
        return currentScope.getName();
    }

    private void beginScope(String scopeName) {
        currentScope = new Scope(scopeName, currentScope);
    }

    private void endScope() {
        System.out.println(currentScope);
        currentScope = currentScope.getParent();
    }

    private boolean isDefined(String name) {
        return currentScope.isDefined(name);
    }

    private void put(String name) {
        if (isDefined(name)) {
            if (currentScope.contains(name)) {
                System.err.println("ERROR: `" + name + "` redefined in " + getScopeName());
            } else {
                System.err.println("WARN: `" + name + "` hidden in " + getScopeName());
                currentScope.put(name);
            }
        } else {
            currentScope.put(name);
        }
    }

    private void use(String name) {
        if (isDefined(name)) {
            if (!currentScope.contains(name)) {
                currentScope.addFreeVar(name);
            }
        } else {
            System.err.println("ERROR: `" + name + "` undefined in " + getScopeName());
        }
    }

    private boolean isDeclaring = false;
    private boolean isMatching = false;

    private void visit(List<? extends ParserRuleContext> list) {
        for (ParserRuleContext ctx : list) {
            visit(ctx);
        }
    }

    @Override
    public Boolean visitTopLevel(MiniHaskellParser.TopLevelContext ctx) {
        beginScope("(top level)");
        visitChildren(ctx);
        endScope();
        return true;
    }

    @Override
    public Boolean visitDirectBind(MiniHaskellParser.DirectBindContext ctx) {
        String ID = ctx.ID().getText();
        put(ID);
        beginScope(ID);
        isDeclaring = true;
        visit(ctx.atom());
        isDeclaring = false;
        visit(ctx.bindBody());
        endScope();
        return true;
    }

    @Override
    public Boolean visitGuardBind(MiniHaskellParser.GuardBindContext ctx) {
        String ID = ctx.ID().getText();
        put(ID);
        beginScope(ID);
        isDeclaring = true;
        visit(ctx.atom());
        isDeclaring = false;
        visit(ctx.guardClause());
        endScope();
        return true;
    }

    @Override
    public Boolean visitBindBody(MiniHaskellParser.BindBodyContext ctx) {
        if (ctx.bindClause().isEmpty()) {
            return visitChildren(ctx);
        }
        beginScope("(" + getScopeName() + " where)");
        visit(ctx.bindClause());
        visit(ctx.expr());
        endScope();
        return true;
    }

    @Override
    public Boolean visitApply(MiniHaskellParser.ApplyContext ctx) {
        use(ctx.ID().getText());
        visit(ctx.expr());
        return true;
    }

    @Override
    public Boolean visitLambda(MiniHaskellParser.LambdaContext ctx) {
        beginScope("(" + getScopeName() + " lambda)");
        isDeclaring = true;
        visit(ctx.atom());
        isDeclaring = false;
        visit(ctx.expr());
        endScope();
        return true;
    }

    @Override
    public Boolean visitLet(MiniHaskellParser.LetContext ctx) {
        beginScope("(" + getScopeName() + " let)");
        visit(ctx.bindClause());
        visit(ctx.expr());
        endScope();
        return true;
    }

    @Override
    public Boolean visitCaseClause(MiniHaskellParser.CaseClauseContext ctx) {
        beginScope("(" + getScopeName() + " case)");
        isMatching = true;
        visit(ctx.pattern);
        isMatching = false;
        visit(ctx.body);
        endScope();
        return true;
    }

    @Override
    public Boolean visitIdentifier(MiniHaskellParser.IdentifierContext ctx) {
        String ID = ctx.ID().getText();

        if (isDeclaring) {
            put(ID);
            return true;
        }

        if (isMatching) {
            if (!isDefined(ID)) {
                put(ID);
            }
            return true;
        }

        use(ID);
        return true;
    }

}
