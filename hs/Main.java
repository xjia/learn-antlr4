import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class Main {
    public static void main(String[] args) throws Exception {
        // create a CharStream that reads from standard input
        ANTLRInputStream input = new ANTLRInputStream(System.in);

        // create a lexer that feeds off of input CharStream
        MiniHaskellLexer lexer = new MiniHaskellLexer(input);

        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // create a parser that feeds off the tokens buffer
        MiniHaskellParser parser = new MiniHaskellParser(tokens);

        ParserRuleContext tree = parser.topLevel();
        System.out.println(tree.toStringTree(parser));
        tree.save(parser, args[0]);
        System.out.println("-----");

        FreeVariableChecker checker = new FreeVariableChecker();
        checker.visit((ParseTree) tree);
    }
}
