grammar MiniHaskell;

topLevel
  : bindClause+
  ;

bindClause
  : ID atom* '=' NL? bindBody # DirectBind
  | ID atom* NL? guardClause+ # GuardBind
  ;

guardClause
  : '|' expr '=' NL? bindBody
  ;

bindBody
  : expr (NL? 'where' bindClause+)? (NL | EOF)
  ;

expr
  : atom                                                  # Atomic
  | expr op=':' expr                                      # Binop
  | expr op=('*' | '/' | 'mod') expr                      # Binop
  | expr op=('+' | '-') expr                              # Binop
  | expr op='++' expr                                     # Binop
  | expr op=('==' | '/=' | '<' | '<=' | '>=' | '>') expr  # Binop
  | expr op='&&' expr                                     # Binop
  | expr op='||' expr                                     # Binop
  | ID expr+                                              # Apply
  | TAG expr+                                             # Build
  | '\\' atom+ '->' expr                                  # Lambda
  | 'if' expr NL? 'then' NL? expr NL? 'else' NL? expr     # If
  | 'case' expr 'of' NL? caseClause+                      # Case
  | 'let' bindClause+ 'in' expr                           # Let
  ;

caseClause
  : pattern=expr '->' body=expr
  ;

atom
  : '(' expr ')'                # Grouping
  | '[' (expr (',' expr)*)? ']' # List
  | ID                          # Identifier
  | TAG                         # Tag
  | NUM                         # Number
  ;

ID
  : 'a'..'z' (Letter | Digit | '_' | '\'')*
  ;

TAG
  : 'A'..'Z' (Letter | Digit | '_' | '\'')*
  ;

NUM
  : Digit+
  ;

fragment Letter
  : 'a'..'z' | 'A'..'Z'
  ;

fragment Digit
  : '0'..'9'
  ;

WS
  : (' ' | '\t')+ { skip(); }
  ;

NL
  : ('\r' | '\n')+
  ;
