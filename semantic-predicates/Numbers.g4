grammar Numbers;

parse
  : atom (',' atom)* EOF
  ;

atom
  : low  { System.out.println("low  = " + $low.text); }
  | high { System.out.println("high = " + $high.text); }
  ;

low
  : { Integer.valueOf(getCurrentToken().getText()) <= 500 }? Number
  ;

high
  : Number
  ;

Number
  : Digit Digit Digit
  | Digit Digit
  | Digit
  ;

fragment Digit
  : '0'..'9'
  ;

WhiteSpace
  : (' ' | '\t' | '\r' | '\n') { skip(); }
  ;

