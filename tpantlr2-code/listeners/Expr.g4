grammar Expr;
s : e ;
e : e op='*' e
  | e op='+' e
  | INT
  ;

MULT: '*' ;
ADD : '+' ;
INT : [0-9]+ ;
WS : [ \t\n]+ -> skip ;
